addEventListener('fetch', (event) => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Respond with hello worker text
 * @param {Request} request
 */
async function handleRequest(request) {
  const body = await request.json()
  return new Response(body.challenge, {
    headers: { 'content-type': 'application/json' },
    status: 200,
  })
}
