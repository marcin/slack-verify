Simple Cloudflare worker to authenticate an endpoint for the [Slack Events
API](https://api.slack.com/apis/connections/events-api#prepare)
